/**
 * Created by akin.akindolani on 3/8/17.
 */
var Tab = function () {
    this.name = '';
    this.description = '';
    this.extension = '';
    this.file_path = '';
    this.font_size = '12px';
    this.created_at = (new Date().toDateString());
    this.code = "";
};