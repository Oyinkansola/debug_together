/**
 * Created by itunu.babalola on 3/2/17.
 */
'use strict';
!function () {
    var app = angular.module('DebugApp.directives');
    app.directive('windowHeight',['$timeout', function ($timeout) {
        function getHeight(element,type) {
             var  height = null;
            if(type == 'id') {
                 height = $('#'+ element+'').height();
            }else{
                  height = $('.'+ element+'').height();
            }

            return height;
        }

        function setHeight(element,type) {
            var height =  $(window).height();
            var nav_height = getHeight('user-nav','id');
            var title_height = getHeight('widget-title','class') ;
          var expected_height =  height - nav_height - title_height;
            if(type == 'id') {
                $('#'+ element+'').height(expected_height);
            }else {
                $('.' + element + '').height(expected_height);
            }
        }
        
        function link(scope,element,attr) {
           setHeight('widget-box','class');
            $(window).resize(function () {
                setHeight('widget-box','class');
            })
        }

        return {
            restrict: 'A',
            link:link
        }
    }]);
    

    app.directive('themeChange',['$timeout','$rootScope','StorageService','$window', function ($timeout,$rootScope, StorageService, $window) {
        function link(scope,element,attrs) {
            StorageService.createDB('color_scheme');
            $(element).find('li').unbind('click').on('click', function () {
                var color = '', text_color = '';
                switch ($(this).data('value')) {
                    case 'white':
                        color = '#FFFFFF';
                        text_color = '#000000';
                        break;
                    case 'black' :
                        color = '#000000';
                        text_color = '#FFFFFF';
                        break;
                    case 'ash' :
                        color = '#808080';
                        text_color = '#e3e3e3';
                        break;
                    case 'grey' :
                        color = '#CCCCCC';
                        text_color = '#000000';
                        break;
                    default:
                        color = '#FFFFFF';
                        text_color = '#000000';

                }
                if(color != '') {
                    StorageService.setDB('color_scheme').saveRecord('color', {color:color, text_color:text_color});
                    $window.location.reload(true);
                }

            });

        }
        return {
            restrict: 'A',
            link:link
        }
    }]);

    app.directive('fontSizeChange',['$rootScope','StorageService',function ($rootScope,StorageService) {
        function link(scope,element,attrs) {
            StorageService.createDB('text-size');
            var fontSize = null;
            $(element).find('li').unbind('click').on('click', function () {
                fontSize =  $(this).data('value');
                if(fontSize != null) {
                    StorageService.setDB('text-size').saveRecord('font',{font:fontSize});
                    $rootScope.$broadcast('fontChange',{font:fontSize});
                }

            })
        }

        return {
            restrict:'A',
            link:link
        }
    }]);

    app.directive('applyFontChange',['$timeout','$rootScope','$window','StorageService',function ($timeout,$rootScope,$window,StorageService) {
        function link(scope,element,attrs) {
            $rootScope.$on('changeFontSize' ,function (event,fontSize) {
                if( fontSize  && fontSize.font != 'undefined') {
                    console.log(fontSize);
                    console.log(fontSize.tabs[fontSize.selected_tab_index]);
                    //fontSize.tabs[fontSize.selected_tab_index].font_size = ''+fontSize.font+'px';
                    StorageService.setDB('tabData').saveRecord('tabs', fontSize.tabs);
                    $('#tab_holder').css('font-size',fontSize.font);
                    //element.css('font-size',fontSize.font);
                }
            });
        }

        return {
            restrict: 'AEC',
            link:link
        }

    }]);
    
    
    app.directive('applyChange',['$timeout','$rootScope','$window', function ($timeout,$rootScope,$window) {
        function link( scope,element,attrs) {
            $rootScope.$on('applyColor', function (event,colorObj) {
               element.css({
                   'background-color':colorObj.color,
                   'color':colorObj.text_color,
                   'background':colorObj.color,
                   'border':0
               });
           });
        }
        return {
            restrict: 'AEC',
            link:link
        }
    }]);

    app.directive('codeEditor',['$timeout','$rootScope','$window', function ($timeout,$rootScope,$window) {

        var colorMap = {
            '#FFFFFF' : 'tomorrow',
            '#000000' : 'twilight'
        };

        var extensionMap = {
            'php':'php',
            'js': 'javascript',
            'json':'json',
            'css':'css',
            'html':'html'
        };

        function link( scope,element,attrs, ngModel) {
            var tab_holder = $('#tab_holder');
            var tab_editor = document.createElement('div');
            var height =  $(window).height();
            var nav_height = getHeight('user-nav','id');
            var title_height = getHeight('widget-title','class') ;
            var expected_height =  height - nav_height - title_height;
            tab_editor.id = attrs.editorId ;// "el"+ (Math.random() * 199);
            tab_editor.setAttribute('class', 'editor_screen');
            tab_editor.innerHTML = "//Write your code here";
            tab_editor.setAttribute('style', 'height:'+expected_height+'px; font-size:inherit');
            $(window).resize(function () {
                height =  $(window).height();
                nav_height = getHeight('user-nav','id');
                title_height = getHeight('widget-title','class') ;
                expected_height =  height - nav_height - title_height;
                tab_editor.setAttribute('style', 'height:'+expected_height+'px; font-size:inherit');
            });
            tab_holder.append(tab_editor);
            var editor = ace.edit(tab_editor.id);
            if(attrs.color in colorMap) {
                editor.setTheme("ace/theme/" + colorMap[attrs.color]);
            }else {
                editor.setTheme("ace/theme/tomorrow");
            }

            if(attrs.file_extension in extensionMap) {
                editor.getSession().setMode("ace/mode/"+ extensionMap[attrs.file_extension]);
            }
            else{
                editor.getSession().setMode("ace/mode/javascript");
            }
            editor.setOption("enableEmmet", true);
            editor.setOptions({
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true
            });

            editor.on('change', function () {
                $timeout(function () {
                    scope.$apply(function () {
                        var value = editor.getValue();
                        ngModel.$setViewValue(value);
                    });
                });
            });

            ngModel.$render = function () {
                editor.setValue(ngModel.$viewValue);
            };

            $rootScope.$on('applyColor', function (event,colorObj) {
                //$window.location.reload(true);
            });
            TogetherJS.reinitialize();

        }
        return {
            scope:true,
            require:'?ngModel',
            restrict: 'A',
            link:link
        };

        function getHeight(element,type) {
            var  height = null;
            if(type == 'id') {
                height = $('#'+ element+'').height();
            }else{
                height = $('.'+ element+'').height();
            }

            return height;
        }
    }]);

    app.directive('fileReader',function () {
        function link(scope,element,attrs) {
            var handleFileChange = scope.$eval(attrs.fileReader);
            element.bind('change',handleFileChange);
        }
        return {
            restrict:'A',
            link:link
        }
    })
}();