/**
 * Created by itunu.babalola on 2/22/17.
 */
'use strict';
    !function () {
    var controllers = angular.module('DebugApp.controllers',[]);
    var services = angular.module('DebugApp.services',[]);
    var directives = angular.module('DebugApp.directives',[]);
    var app = angular.module('DebugApp',['ngRoute','ngResource','angularFileUpload','DebugApp.controllers','DebugApp.services','DebugApp.directives']);
    app.config(['$routeProvider','$interpolateProvider','$httpProvider','$locationProvider', function ($routeProvider,$interpolateProvider,$httpProvider,$locationProvider) {
        $routeProvider.when('/dashboard/:tab_index?',{
            templateUrl :'/app/templates/dashboard.phtml'
        });

        $routeProvider.otherwise({
            redirectTo :'/dashboard'
        });

        $locationProvider.html5Mode(true);
    }]);
    }();