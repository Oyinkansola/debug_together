/**
 * Created by itunu.babalola on 2/2/17.
 */
'use strict';
!function() {
    var userService = angular.module('DebugApp.services');
    userService.service('ResponseService',['$rootScope',function ($rootScope) {

        return {
            Brew: Brew
        };

        function Brew(response) {
            return {
                isOk:isOk,
                getData:getData,
                getError:getError
            };

            function isOk() {
                return (response.status == 'success' &&
                response.data &&
                ( response.data.response || response.data.message)
                ) ? true : false;
            }

            function getData() {
                if(isOk()) {

                    return response.data.response || response.data.message;
                }
                return false;
            }

            function getError() {
                if(!isOk()) {
                   return response.data.message;
                }
                return "A network error has occured";
            }
        }
    }]);

    userService.service('TogetherJSService', [function () {
        return {
            sendData:function(type,data,callback){
                console.log('test');
                if(!TogetherJS.running){return;}
                if(typeof TogetherJS != 'undefined'){
                    TogetherJS.send({
                        type:type,
                        data:data,
                        targetFxn:callback
                    });
                }
            }
        }
    }]);

    userService.service('StorageService', ['$rootScope', function ($rootScope) {
        var __local_storage = localStorage;


        return {
            createDB:createDB,
            setDB:setDB
        };

        function createDB(db_name) {
            if(__local_storage) {
                if(!__local_storage.getItem(db_name)) {
                    __local_storage.setItem(db_name, JSON.stringify({}));
                }
            }
        }

        function setDB(db_name) {
            createDB(db_name);
            return {
                saveRecord:saveRecord,
                getRecord:getRecord,
                hasRecord: hasRecord
            };

            function saveRecord(data_key, data) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        jRecord[data_key] = data;
                        __local_storage.setItem(db_name, JSON.stringify(jRecord));
                    }catch (e) {}
                }
            }

            function getRecord(key) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                       if(key in jRecord) { return jRecord[key];}
                    }catch (e) {}
                }
                return false;
            }

            function hasRecord() {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        return Object.keys(jRecord).length > 0;
                    }catch (e) {
                        return false;
                    }
                }
                return false;
            }
        }

    }]);
}();