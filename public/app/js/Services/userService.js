/**
 * Created by itunu.babalola on 2/25/17.
 */
'use strict';
!function () {
    var app = angular.module('DebugApp.services');
    app.service('UserService',['$resource', function ($resource) {
        var req1 = $resource('/logout' , {} ,{
            logout : {method :'GET'}
        });

        var req2 = $resource('api/:action', {},{
            startSession: {method:'POST',isArray:false, params:{action:'start_togetherjs_session'}}
        });


        return{
          logout:logout,
            startSession:startSession
        };


        function logout() {
           return  req1.logout().$promise;
        }
        function startSession (params) {
            return req2.startSession(params).$promise;
        }
    }]);
}();