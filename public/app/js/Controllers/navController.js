/**
 * Created by itunu.babalola on 2/25/17.
 */
'use strict';
!function () {
    var app = angular.module('DebugApp.controllers');
        app.controller('navController',['$scope','$window','ResponseService','MainService','UserService',function ($scope,$window,ResponseService,MainService,UserService) {
            var vm = this;
            vm.user = __User;
            vm.signOut = signOut;
            vm.sendInvite = sendInvite;

            function signOut() {
                UserService.logout().then(function (response) {
                    var resp = ResponseService.Brew(response);
                    if(response.status == 'success') {
                        $window.location.href = 'http://dev.debug-together.it/login';
                    }
                })
            }


            function sendInvite() {
                MainService.Modal().open('/app/templates/modal-templates/invite.html',{
                    title:'Invite Your Friend',
                    showDefaultButtons: true,
                    mscope:vm
                },[{
                    label:'Invite',
                    name:'Proceed',
                    onClick : function (params) {
                        console.log(params);
                    }
                }])
            }
        }]);
}();