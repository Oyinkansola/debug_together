/**
 * Created by itunu.babalola on 3/2/17.
 */
'use strict';
!function () {
    var app = angular.module('DebugApp.controllers');
    app.controller('StageController',['$timeout','$scope', '$rootScope','StorageService','ResponseService','MainService','$routeParams','TogetherJSService','$window','UserService' ,function ($timeout,$scope,$rootScope, StorageService,ResponseService,MainService,$routeParams, TogetherJSService, $window,UserService) {
        var vm = this;
        var colorObj = StorageService.setDB('color_scheme').getRecord('color');
        var font = StorageService.setDB('text-size').getRecord('font');
        var tabDB = StorageService.setDB('tabData');
        vm.file ='';
        vm.selected_tab_index = typeof $routeParams.tab_index == 'undefined' ? 0 : $routeParams.tab_index;
        vm.tabs = [];
       // var font =  (!(vm.selected_tab_index in vm.tabs) || typeof vm.tabs[vm.selected_tab_index].font_size == 'undefined') ? '12px': vm.tabs[vm.selected_tab_index].font_size;

        if(tabDB.hasRecord()) {
            vm.tabs = tabDB.getRecord('tabs');
        }else {
            addNewTab();
        }
        switchTab(vm.selected_tab_index);
        
        vm.addNewTab = addNewTab;
        vm.switchTab = switchTab;

        vm.color = colorObj.color;
        vm.text_color = colorObj.text_color;
        $timeout(function () {
            $rootScope.$broadcast('applyColor',colorObj);
            $rootScope.$broadcast('changeFontSize', {font:font.font,tabs:vm.tabs,selected_tab_index:vm.selected_tab_index});
            getUserInviteUrl();
        },1);

        $rootScope.$on('themeChange', function (event,colorObj) {
            vm.color = colorObj.color;
            vm.text_color = colorObj.text_color;
            $rootScope.$broadcast('applyColor',{color:vm.color, text_color:vm.text_color});
        });
        $scope.$on('$routeChangeStart', function (event,routeData) {
            //vm.tabs[vm.selected_tab_index].code = "hello bro from "+vm.selected_tab_index;
            StorageService.setDB('tabData').saveRecord('tabs', vm.tabs);
            //event.preventDefault();
        });

        $rootScope.$on('fontChange', function (event,font) {
            vm.font_size = font.font;
            $rootScope.$broadcast('changeFontSize', {font:vm.font_size,tabs:vm.tabs,selected_tab_index:vm.selected_tab_index});
        });

        $rootScope.uploadFile = function (event) {
            console.log(event);
            var file = event.target.files[0];
            var reader = new FileReader();
            var content = null;
            reader.onload = function(event) {
                // The file's text will be printed here
                content = event.target.result;
                addNewTab(file.name,content);
            };

            reader.readAsText(file);
        };

        
        function addNewTab(tab_name,content) {
            var new_tab = new Tab();
            if(tab_name && content) {
                var file_extension = tab_name.split('.').pop();
                new_tab.name = tab_name;
                new_tab.code = content;
                new_tab.extension = file_extension;
            }
            else{
                new_tab.name = "Tab " + vm.tabs.length;
            }
            vm.tabs.push(new_tab);
            StorageService.setDB('tabData').saveRecord('tabs', vm.tabs);
            $rootScope.$broadcast('applyColor',colorObj);
            $timeout(function () {
                TogetherJS.reinitialize();
            },1);
            var index = vm.tabs.length - 1;
            $window.location.href = '/dashboard/' +index;
        }

        function switchTab(index) {
            vm.selected_tab_index = index;
            console.log(TogetherJSService);
            TogetherJSService.sendData('tab-change', {tab:vm.tabs[index], tab_index:index}, function (response) {
                console.log("Data Hunt");
                console.log(response);
            });
        }



        function getUserInviteUrl() {
            if(typeof  TogetherJS != 'undefined') {
                TogetherJS.on('ready', function () {
                    var user_invite_url = TogetherJS.shareUrl();
                    var user_invite_code = user_invite_url.split('=').pop();
                    UserService.startSession({session_code:user_invite_code}).then(function (response) {

                    });

                });

            }
        }


        if(typeof TogetherJS != 'undefined'){

            TogetherJS.on("close", function () {
                console.log('codebug:closed');
            });

            TogetherJS.hub.on('open-tab',function(payload){
                console.log(payload);
            });
            TogetherJS.hub.on('close-tab',function(payload){
                console.log(payload);
            });
            TogetherJS.hub.on('tab-change',function(payload){
                $window.location.href = '/dashboard/' + payload.data.tab_index;
            });
        }

    }]);
}();