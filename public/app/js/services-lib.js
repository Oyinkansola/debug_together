/**
 * Created by itunu.babalola on 2/3/17.
 */
'use strict';
!function() {
    var mainService = angular.module('DebugApp.services');
    mainService.service('MainService',['$resource','$rootScope',function ($resource,$rootScope) {

        return {
            Modal:Modal,
            Alert:Alert
        };


        function Modal() {
            return {
                open:function (template_url,data,buttons) {
                    if(typeof  template_url != 'undefined' &&  buttons.length > 0   ){
                        $rootScope.$broadcast('modal',{template:template_url,data :data, buttons: buttons});
                    }
                    else{
                        $rootScope.$broadcast('modal',{template:template_url,data :data});
                    }
                },
                close:function () {
                    $rootScope.$broadcast('modal',{template:null,data:null});
                }
            }
        }


        function Alert() {
            return {
                open : function (message,config) {
                    if(typeof message != 'undefined' && typeof message != 'object' && message.trim().length > 0) {
                        $rootScope.$broadcast('alert' ,{message: message,config:config,show:true})
                    }

                },
                close : function (alert_id) {
                    $rootScope.$broadcast('alert',{show:false})
                }
            }
        }
    }]);
}();
/**
 * Created by itunu.babalola on 2/25/17.
 */
'use strict';
!function () {
    var app = angular.module('DebugApp.services');
    app.service('UserService',['$resource', function ($resource) {
        var req1 = $resource('/logout' , {} ,{
            logout : {method :'GET'}
        });

        var req2 = $resource('api/:action', {},{
            startSession: {method:'POST',isArray:false, params:{action:'start_togetherjs_session'}}
        });


        return{
          logout:logout,
            startSession:startSession
        };


        function logout() {
           return  req1.logout().$promise;
        }
        function startSession (params) {
            return req2.startSession(params).$promise;
        }
    }]);
}();
/**
 * Created by itunu.babalola on 2/2/17.
 */
'use strict';
!function() {
    var userService = angular.module('DebugApp.services');
    userService.service('ResponseService',['$rootScope',function ($rootScope) {

        return {
            Brew: Brew
        };

        function Brew(response) {
            return {
                isOk:isOk,
                getData:getData,
                getError:getError
            };

            function isOk() {
                return (response.status == 'success' &&
                response.data &&
                ( response.data.response || response.data.message)
                ) ? true : false;
            }

            function getData() {
                if(isOk()) {

                    return response.data.response || response.data.message;
                }
                return false;
            }

            function getError() {
                if(!isOk()) {
                   return response.data.message;
                }
                return "A network error has occured";
            }
        }
    }]);

    userService.service('TogetherJSService', [function () {
        return {
            sendData:function(type,data,callback){
                console.log('test');
                if(!TogetherJS.running){return;}
                if(typeof TogetherJS != 'undefined'){
                    TogetherJS.send({
                        type:type,
                        data:data,
                        targetFxn:callback
                    });
                }
            }
        }
    }]);

    userService.service('StorageService', ['$rootScope', function ($rootScope) {
        var __local_storage = localStorage;


        return {
            createDB:createDB,
            setDB:setDB
        };

        function createDB(db_name) {
            if(__local_storage) {
                if(!__local_storage.getItem(db_name)) {
                    __local_storage.setItem(db_name, JSON.stringify({}));
                }
            }
        }

        function setDB(db_name) {
            createDB(db_name);
            return {
                saveRecord:saveRecord,
                getRecord:getRecord,
                hasRecord: hasRecord
            };

            function saveRecord(data_key, data) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        jRecord[data_key] = data;
                        __local_storage.setItem(db_name, JSON.stringify(jRecord));
                    }catch (e) {}
                }
            }

            function getRecord(key) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                       if(key in jRecord) { return jRecord[key];}
                    }catch (e) {}
                }
                return false;
            }

            function hasRecord() {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        return Object.keys(jRecord).length > 0;
                    }catch (e) {
                        return false;
                    }
                }
                return false;
            }
        }

    }]);
}();