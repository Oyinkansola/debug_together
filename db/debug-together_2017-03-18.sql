# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.33)
# Database: debug-together
# Generation Time: 2017-03-18 10:09:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table togetherjs_session_invites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `togetherjs_session_invites`;

CREATE TABLE `togetherjs_session_invites` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `togetherjs_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `invitation_token` varchar(200) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table together_js
# ------------------------------------------------------------

DROP TABLE IF EXISTS `together_js`;

CREATE TABLE `together_js` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_code` varchar(50) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `together_js` WRITE;
/*!40000 ALTER TABLE `together_js` DISABLE KEYS */;

INSERT INTO `together_js` (`id`, `user_id`, `session_code`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,'ULGPvz3hO3',0,'2017-03-18 10:29:27','2017-03-18 10:29:27');

/*!40000 ALTER TABLE `together_js` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_token` text NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `image_url` varchar(200) DEFAULT '',
  `network_type` varchar(20) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `id_token`, `name`, `email`, `image_url`, `network_type`, `created_at`, `updated_at`)
VALUES
	(1,'eyJhbGciOiJSUzI1NiIsImtpZCI6IjcwYWFlMDk3YmUwZmEyYTk5OTg1MmQ1N2E0ODBlNGNhZDZiZGI4MWMifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiaWF0IjoxNDg3OTU1ODY2LCJleHAiOjE0ODc5NTk0NjYsImF0X2hhc2giOiJRY1V0cEJWeWlRbU52em9LLTlMZTdRIiwiYXVkIjoiMjg0Nzg5MzAyNjg5LWxydWNocnNybDZkZHIwbG9vZjM2YmFvZjVzcTFvZzl0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTEzODAwOTQwNTg5NTY5NzYyOTg3IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF6cCI6IjI4NDc4OTMwMjY4OS1scnVjaHJzcmw2ZGRyMGxvb2YzNmJhb2Y1c3Exb2c5dC5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoib3lpbmthbnNvbGFhcml5b0BnbWFpbC5jb20iLCJuYW1lIjoiT3lpbmthbnNvbGEgQXJpeW8iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDUuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1oTktqUUpLbmxKay9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBNC9IU0dIRlJXdk9zUS9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiT3lpbmthbnNvbGEiLCJmYW1pbHlfbmFtZSI6IkFyaXlvIiwibG9jYWxlIjoiZW4ifQ.ByxSPv7qx7949CxIX0p8GeJzeSXKzq8cBnxhoNHZSYjqPyLTQIYfCWm8u2y3AxMH9YHJoICeCF1fVn3uMhjXfpX9nSEbf8fnRRnqFjQKRbespahILILjdrg9NeRxUTK5IQdPik-akhHew3lB1l1V6QwyQTLdeyglB1ZhZKs_GAx7nlvnZqlm9v50vFQNM3cmuK1Kki6nTvyHC8hYR1DiGgWHoKeNQvgxLed2bkzamfGPby-2CIE7SKriwvBl9wT0tUMNzdIQk0FrW4Qq59OARuw9tXdgIPkilBhRYoHWVRN0WV_qSu2rY7CBQYLunx58l0A6i0N6kSfxIwmevpC9_Q','Oyinkansola Ariyo','oyinkansolaariyo@gmail.com','https://lh5.googleusercontent.com/-hNKjQJKnlJk/AAAAAAAAAAI/AAAAAAAAAA4/HSGHFRWvOsQ/s96-c/photo.jpg','google','2017-02-24 18:04:26','2017-02-24 18:04:26'),
	(2,'eyJhbGciOiJSUzI1NiIsImtpZCI6IjcwYWFlMDk3YmUwZmEyYTk5OTg1MmQ1N2E0ODBlNGNhZDZiZGI4MWMifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiaWF0IjoxNDg3OTY3MDExLCJleHAiOjE0ODc5NzA2MTEsImF0X2hhc2giOiI0QWo5cUxXby1UMTJXRzh6cEhCMV93IiwiYXVkIjoiMjg0Nzg5MzAyNjg5LWxydWNocnNybDZkZHIwbG9vZjM2YmFvZjVzcTFvZzl0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAwMzg1NTg0NTc2ODI1MTc0NDYwIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF6cCI6IjI4NDc4OTMwMjY4OS1scnVjaHJzcmw2ZGRyMGxvb2YzNmJhb2Y1c3Exb2c5dC5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImhkIjoia29uZ2EuY29tIiwiZW1haWwiOiJpdHVudS5iYWJhbG9sYUBrb25nYS5jb20iLCJuYW1lIjoiSXR1bnUgQmFiYWxvbGEiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1lRDRieDdid0FhNC9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQW9tdlYwRVo5cDF1cU1qN0Eya092SHJGY3RPN19RbmdBL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJJdHVudSIsImZhbWlseV9uYW1lIjoiQmFiYWxvbGEiLCJsb2NhbGUiOiJlbiJ9.nlYR2sa1sKN1ZmDNLtwKsMxGIKx3a3VAardWUBUQbNSP9bFK6-ZK5Wf04mRMffiZjbI6TTXl7eKU1K7Yf3IoUlixgxV7lb9QciA-W7qdUFRd_CLvCHrUb8Nr7Li4Nhc_QmGqzaqsl9MBZY5adTK1meAUED4s4J_p2J5CzXmKW0I1mpNBDmeG6nWe0mM8Qz51vsbh_zYzj87vDytIYNbXyvDc1FQzM-acifIQSltd12rNCDV0nBoAkIN9_kZUfjTxG0RZnV3FSzpEAHrDOoK9h9DTLAfKVFviir0JjOR8VbUAqVMIbVQm_oP3iMFFWuv0jkpodjhhkXiNc4F48h4COg','Itunu Babalola','itunu.babalola@konga.com','https://lh4.googleusercontent.com/-eD4bx7bwAa4/AAAAAAAAAAI/AAAAAAAAAAA/AAomvV0EZ9p1uqMj7A2kOvHrFctO7_QngA/s96-c/photo.jpg','google','2017-02-24 21:10:11','2017-02-24 21:10:11');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
