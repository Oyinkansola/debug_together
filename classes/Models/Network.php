<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/24/17
 * Time: 8:57 AM
 */

namespace Models;
use Illuminate\Database\Eloquent\Model as EModel;

class Network extends EModel
{
    protected  $fillable = ['network_name'];
    protected $guarded = ['id'];


}