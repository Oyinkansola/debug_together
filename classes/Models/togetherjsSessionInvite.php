<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 3/18/17
 * Time: 9:14 AM
 */

namespace Models;
use Illuminate\Database\Eloquent\Model as EModel;

class togetherjsSessionInvite extends EModel
{
    protected $fillable = ['user_id','togetherjs_id','invitation_token'];
    protected $guarded = ['id'];

    public function togetherJs() {
        return $this->belongsTo('Models\togetherJs');
    }

    public static function addInvite($data) {
        $new_invite = new togetherjsSessionInvite($data);
        $new_invite->save();
        return $new_invite;
    }

    public static function checkIfInvited($token) {
        $invitee = togetherjsSessionInvite::where('invitation_token',$token)->first();
        return $invitee;
    }

    public static function addInvitesId($token,$id) {
        $check = togetherjsSessionInvite::where('invitation_token',$token)->first()->update(['user_id'=>$id]);
        if($check) {
            return true;
        }else{
            return false;
        }
    }
}