<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/23/17
 * Time: 7:23 PM
 */

namespace Models;
use Illuminate\Database\Eloquent\Model as EModel;

class User extends EModel
{
    protected  $fillable =['email','id_token','network_type','name','image_url'];
    protected  $guarded = ['id'];

    public  function togetherJsSession() {
        return $this->hasMany('Models\togetherJsSession');
    }

    public  static  function checkUser($user) {

             $check = User::where('email',$user['email'])->first();
             if($check == null) {
                 $new_user = new User($user);
                 $new_user->save();
                 return $new_user;
             }else{
                 return $check;
             }
        }
}