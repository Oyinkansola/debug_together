<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/24/17
 * Time: 8:31 AM
 */

namespace Models;
use Illuminate\Database\Eloquent\Model as EModel;

class togetherJs extends EModel
{
    protected  $fillable = ['user_id','session_code','status','date'];
    protected  $guarded = ['id'];

    public  function User() {
        return $this->belongsTo('Models\User');
    }

    public  function togetherjsSessionInvite() {

        return $this->hasMany('Models\togetherjsSessionInvite');
    }

    public  static  function addSession($data) {
        $check = togetherJs::where('session_code',$data['session_code'])->first();
        if($check ==  null) {
            $new_session = new  togetherJs($data);
            $new_session->save();
            return $new_session;
        }else{
            return $check;
        }

    }
}