<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/23/17
 * Time: 7:17 PM
 */

namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils;
use Models\User;

class UserController extends BaseController
{
    public  function index(Request $request,Response $response, $args)
    {
        $this->container->renderer->render($response,'login.phtml',[]);
        return $response;
    }

    public  function logout(Request $request,Response $response, $args)
    {
        Utils::unsetSessions();
       return $response->withJson(Utils::setSuccessMessage('100','loggedout'));

    }



}