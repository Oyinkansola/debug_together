<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 3/16/17
 * Time: 10:26 PM
 */

namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils;

class Message
{

        public  function __construct($mailer,$template)
        {
            $mailer->addAddress($template['recipient_email']);
            $mailer->Subject = 'Codebug Invites';
            $mailer->Body = $template['name'].' has invited you to join their session on Codebug:<a href='.$template['url'].' >Join them by clicking</a>';
        }

}