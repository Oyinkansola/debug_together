<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/25/17
 * Time: 8:38 AM
 */

namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use GuzzleHttp\Client as guzzle;
use Models\User;
use Utils\Utils;
use Models\togetherjsSessionInvite;

class GuzzleController extends BaseController
{
    public function githubLogin(Request $request,Response $response,$args) {
        try{
            $valid = null;
            $invitation_token = $args['invitation_token'];
            if($invitation_token) {
                $valid = togetherjsSessionInvite::checkIfInvited($invitation_token);
            }
            $query = $request->getQueryParams();
            $client = new guzzle();
            $settings = $this->container->get('settings');
            $github = $settings['github'];
            $answer = $client->post('https://github.com/login/oauth/access_token',array(
                'query' => [
                    'client_id'=> $github['Client ID'] ,
                    'client_secret' => $github['Client Secret'],
                    'code' => $query['code'],
                    'state' => $github['state']
                ],
            ),'json');

            $data =  \GuzzleHttp\Psr7\parse_query($answer->getBody()->getContents(), true);

            if(isset($data['access_token'])) {

                $user = $client->get('https://api.github.com/user?access_token='.$data['access_token']);
                $userData = json_decode($user->getBody()->getContents(),false);

                $new_user = ['id_token' => $userData->id,'name'=> $userData->name,'email'=>$userData->email,'image_url'=>$userData->avatar_url];
                $check =  User::checkUser($new_user);

                if($check) {

                    unset($new_user['id_token']);
                    Utils::setSession('user', $check);
                    Utils::registerSession();
                    if($valid) {
                        togetherjsSessionInvite::addInvitesId($invitation_token,$check['id']);
                        return $response->withStatus(302)->withHeader('location', '/dashboard#&togetherjs='.$invitation_token);
                    }
                    else{
                        return $response->withStatus(302)->withHeader('location', '/dashboard');
                    }

                }
            }

        }catch (\Exception $e) {
            Utils::setFlashMessage("Login not successful. Please ensure you have a publicly accessible email attached to this account");
            return $response->withStatus(302)->withHeader('location', '/login');

        }

    }

    public  function googleLogin(Request $request,Response $response,$args)
    {
        try{
            $valid = null;
            $invitation_token = $args['invitation_token'];
            if($invitation_token) {
                $valid = togetherjsSessionInvite::checkIfInvited($invitation_token);
            }
            $settings = $this->container->get('settings');
            $gClient = $this->container->get('googleClient');
            $gClient->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/googlelogin');
            $gClient->addScope("email");
            $gClient->addScope("profile");
            $service = new \Google_Service_Oauth2($gClient);
            $query = $request->getQueryParams();
            if (! isset($query['code'])) {

                $auth_url = $gClient->createAuthUrl();
                return $response->withStatus(302)->withHeader('location',filter_var($auth_url, FILTER_SANITIZE_URL) );

            }
            else {

                $gClient->authenticate($_GET['code']);
                $access_token = $gClient->getAccessToken();
                $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/dashboard';
                if(isset($access_token)) {

                    $gClient->setAccessToken($access_token);
                    $user = $service->userinfo->get();
                    $userData = ['email'=> $user->email,'name'=>$user->name,'image_url'=>$user->picture,'id_token'=>$access_token];
                    $check =  User::checkUser($userData);
                    if($check) {

                        unset($userData['id_token']);
                        Utils::setSession('user', $check);
                        Utils::registerSession();
                        if($valid) {
                            togetherjsSessionInvite::addInvitesId($invitation_token,$check['id']);
                            return $response->withStatus(302)->withHeader('location', $redirect_uri.'#&togetherjs='.$invitation_token);
                        }
                        else{
                            return $response->withStatus(302)->withHeader('location', $redirect_uri);
                        }

                    }

                }

            }


        }catch (\Exception $e) {
            Utils::setFlashMessage("Login not successful. Please ensure you have a publicly accessible email attached to this account");
            return $response->withStatus(302)->withHeader('location', '/login');
        }


    }
}