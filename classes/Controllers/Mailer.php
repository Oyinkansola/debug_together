<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 3/15/17
 * Time: 11:00 PM
 */

namespace Controllers;
use Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils;


class Mailer extends BaseController
{
    public   static function sendMail($template,$mailer) {
        try{
            new Message($mailer,$template);
            return $mailer->send();
        }catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}