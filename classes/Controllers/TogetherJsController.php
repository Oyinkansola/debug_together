<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 3/14/17
 * Time: 6:59 PM
 */

namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils;
use Models\togetherJs;
use Models\togetherjsSessionInvite;

class TogetherJsController extends BaseController
{
    public  function createNewSession( Request $request,Response $response,$args) {
        try{
            $user = $this->user;
            $user_id = $user['id'];
            $data = $request->getParsedBody();
            if($data) {
                $data['user_id'] = $user_id;
                $new_session = togetherJs::addSession($data);
                $this->user['togetherjs_id'] = $new_session['id'];
                $this->user['session_code'] = $new_session['session_code'];
                if($new_session) {
                    return $response->withJson(Utils::setSuccessMessage('Together Js Session started successfully',$new_session));
                }
                else{
                    return $response->withJson(Utils::setErrorMessage('101','Couldn\'t register a togetherJs session'));
                }

            }else{
                return $response->withJson(Utils::setErrorMessage('101','You have to send the post body'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('101',$e->getMessage()));
        }
    }

    public function inviteSomeone(Request $request,Response $response,$args) {
        try{
            //i would be expecting the invite's email, and the users tabdata in local storage
            $user = $this->user;
            $mailer = $this->container->get('mailer');
            $data = $request->getParsedBody();
            $template = [];
            if($data) {
                $data['user_id'] =$user['id'];
                $data['togetherjs_id'] = $user['togetherjs_id'];
                $data['invitation_token'] = bin2hex(openssl_random_pseudo_bytes(16));
                $invite = togetherjsSessionInvite::addInvite($data);
                $template['recipient_email'] = $data['recipient_email'];
                $template['name'] = $user['name'];
                $template['url'] = 'dev.debug-together.it/login/'.$data['invitation_token'];
                $send = Mailer::sendMail($template,$mailer);
                if($invite && $send) {
                    return $response->withJson(Utils::setSuccessMessage('You have invited succesfully',$invite));
                }else{
                    return $response->withJson(Utils::setErrorMessage('101','Couldn\'t  send invite'));
                }
            }
            else{
                return $response->withJson(Utils::setErrorMessage('101','You have to send the post body'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('101',$e->getMessage()));
        }
    }
}