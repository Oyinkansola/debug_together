<?php

/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/23/17
 * Time: 7:13 PM
 */
namespace Controllers;
use \Interop\Container\ContainerInterface;
use Utils\Utils;

 abstract class BaseController
{
    protected  $container;

    public function __construct(ContainerInterface $c)
    {
        $this->container = $c;
        if(Utils::isLoggedIn()) {
            $this->user = Utils::setSession('user');
        }
    }
 }