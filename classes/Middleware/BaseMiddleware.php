<?php

/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/23/17
 * Time: 7:14 PM
 */

namespace Middleware;
use \Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface;


abstract  class BaseMiddleware
{
    protected  $container;

    public  function __construct(ContainerInterface $c)
    {
        $this->container = $c;
    }
}