<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/23/17
 * Time: 7:22 PM
 */

namespace Middleware;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils;

class AuthMw extends BaseMiddleware
{
    public function __invoke(Request $request,Response $response,$next)
    {
        // TODO: Implement __invoke() method.
        if(!Utils::isLoggedIn()) {
            $this->container->user = Utils::setSession('user');
            return $response->withStatus(302)->withHeader('location','/login');
        }
        $response = $next($request,$response);
        return $response;
    }
}