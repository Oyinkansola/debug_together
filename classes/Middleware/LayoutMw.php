<?php
/**
 * Created by PhpStorm.
 * User: itunu.babalola
 * Date: 2/23/17
 * Time: 7:22 PM
 */

namespace Middleware;

 use \Psr\Http\Message\ServerRequestInterface as Request;
 use \Psr\Http\Message\ResponseInterface as Response;
 use Utils\Utils;
class LayoutMw extends BaseMiddleware
{
    public  function __invoke(Request $request,Response $response,$next)
    {
          $configurations = $this->container->get('settings');
        // TODO: Implement __invoke() method.
        if(!Utils::isLoggedIn()) {
            $this->container->renderer->render($response,'no-auth-header.phtml',['configurations'=>$configurations]);
        }
        else{
        $this->container->renderer->render($response,'auth-header.phtml',['configurations'=>$configurations]);
        }

        $response = $next($request,$response);


        if(!Utils::isLoggedIn()) {
            $this->container->renderer->render($response,'no-auth-footer.phtml',[]);
        }else{
            $this->container->renderer->render($response,'auth-footer.phtml',[]);
        }
        return $response;
    }
}