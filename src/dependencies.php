<?php
// DIC configuration
use \Illuminate\Database\Capsule\Manager as Capsule;
$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

//database
$container['db'] = function ($c){
    $capsule = new Capsule();
    $capsule->addConnection($c->get('settings')['db']);
    $capsule->setAsGlobal();
    $capsule->connection();
    $capsule->bootEloquent();
    return $capsule;
};

//guzzleClient
$container['guzzleClient'] = function ($c) {
    $guzzle = new \GuzzleHttp\Client([
        'max'             => 5,
        'strict'          => false,
        'referer'         => true,
        'protocols'       => ['http', 'https'],
        'track_redirects' => false,
        /*'base_uri'        => 'http://162.243.110.226/transwire-service/'*/
    ]);

  return $guzzle;
};

$container['googleClient'] = function ($c) {
    $googleClient = new Google_Client();
    $googleClient->setAuthConfig('../auth_config.json');
    $googleClient->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);
   return $googleClient;
};

$container['mailer'] = function ($c) {
  $mailer = new PHPMailer();
    $mailer->Host = 'dev.debug-together.it';
    $mailer->SMTPAuth = true;                 //  set false for localhost
    $mailer->SMTPSecure = 'ssl';              // set blank for localhost
    $mailer->Port = 25;                           // 25 for local host  host smtp port
	$mailer->Username = '';    //
	$mailer->Password = '';
	$mailer->isHTML(true);
	return $mailer;
};
$container->get('db');
$container->get('guzzleClient');
$container->get('googleClient');
