<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        //database settings

        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'debug-together',
            'username' => 'root',
            'password' => 'root',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',

        ],
        //github configurations
        'github' => [
            'Client ID'=>'480ec67406d0e7a79e1d',
            'Client Secret'=>'682fc9254da264f5b853b2efa61ba9b41604c6d4',
            'profile email'=>'oyinkansolaariyo@gmail.com',
            'redirect_uri' =>'http://dev.debug_together.it/github',
            'scope' =>'user:email',
            'state' =>'itunu' ,
            'allow_signup' =>'false'
        ],
        'google' => [
            'Client ID'=>'284789302689-lruchrsrl6ddr0loof36baof5sq1og9t.apps.googleusercontent.com',
            'Client Secret'=>'QXVBN5M1e83533PdQVTqMkVF',
            'profile email'=>'oyinkansolaariyo@gmail.com',
            'redirect_uri' =>'http://dev.debug_together.it/google'
        ],

    ],
];
