<?php
// Routes

$app->get('/login','\Controllers\UserController:index')->add(new \Middleware\LayoutMw($container));
$app->get('/login/:invitation_token','\Controllers\UserController:index')->add(new \Middleware\LayoutMw($container));
$app->get('/logout','\Controllers\UserController:logout');
$app->get('/github','\Controllers\GuzzleController:githubLogin');
$app->get('/googlelogin','\Controllers\GuzzleController:googleLogin');
$app->group('/api',function() use ($app, $container){
   // $app->post('/login','\Controllers\UserController:loginController');
    $app->post('/start_togetherjs_session','\Controllers\TogetherJsController:createNewSession');
    $app->post('/send_invite','\Controllers\TogetherJsController:inviteSomeone');
    $app->post('/mailer','\Controllers\Mailer:sendMail');
})->add(new \Middleware\AuthMw($container));
$app->get('/{path:(?!(?:login)(?:logout)(?:register)).*}', function ($request, $response, $args) {
    // Render index view
    //handing over to angular
    return $this->renderer->render($response, 'index.phtml', $args);
})->add(new \Middleware\LayoutMw($container))->add(new \Middleware\AuthMw($container));
